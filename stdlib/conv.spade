/// Truncate the high order bits of the supplied integer. If the result is
/// larger than the input, an error is emitted
// This is special cased by the compiler. The actual type, if Spade was able to express
// it itself would be trunc<#N, #M>(x: Number<N>) -> Number<M>.
fn trunc<N, M>(x: N) -> M __builtin__

/// Sign extend the provided integer. Gives a compiler error if the output is smaller
/// than the input
fn sext<#N, #M>(x: int<N>) -> int<M> __builtin__

/// Zero extend the provided integer. Gives a compiler error if the output is smaller
/// than the input
fn zext<#N, #M>(x: uint<N>) -> uint<M> __builtin__

// This is special cased by the compiler. The actual type, if Spade was able to express
// it itself would be concat<#N, #M>(x: Number<N>, y: Number<M>) -> Number<N + M>.
fn concat<N, M, K>(x: N, y: M) -> K __builtin__

// Casts a `bit` to a `bool`. low maps to false, high maps to true, but HIGHIMP is undefined.
// In practice, in a simulator, it will map to Z
fn bit_to_bool(b: bit) -> bool {
    unsafe::unsafe_cast(b)
}



// Completely reverses the order of the elements in the `in` array
// For example, [1, 2, 3] becomes [3, 2, 1]
fn flip_array<T, #N>(in: [T; N]) -> [T; N] __builtin__

// Interpret an int as the raw bit values. output[0] is the lsb of the integer
// nad output[N] is the MSB
//
// Note that this means that 0b1100 will be [false, false, true, true] since arrays
// are written lsb first, while integers are written msb first
fn int_to_bits<#N>(input: int<N>) -> [bool; N] {
    unsafe::unsafe_cast(input)
}

fn bits_to_int<#N>(input: [bool; N]) -> int<N> {
    unsafe::unsafe_cast(input)
}

// Interpret a uint as the raw bit values. output[0] is the lsb of the integer
// nad output[N] is the MSB
//
// Note that this means that 0b1100 will be [false, false, true, true] since arrays
// are written lsb first, while integers are written msb first
fn uint_to_bits<#N>(input: uint<N>) -> [bool; N] {
    unsafe::unsafe_cast(input)
}
fn bits_to_uint<#N>(input: [bool; N]) -> uint<N> {
    unsafe::unsafe_cast(input)
}

// Cast an unsigned integer to a signed integer by re-interpreting the bits.
fn uint_to_int <#N>(input: uint<N>) -> int<N> {
    unsafe::unsafe_cast(input)
}
// Cast a signed integer to an unsigned integer by re-interpreting the bits.
fn int_to_uint <#N>(input: int<N>) -> uint<N> {
    unsafe::unsafe_cast(input)
}

mod unsafe {
    // Reinterprets a value as another value of the same size. It is up to the caller
    // to ensure that the resulting value is valid
    //
    // **NOTE**: The representation of compound types is generally undefined and you should
    // not rely on it being fixed between compiler versions.
    // If you still want to use this function on compound types, the representation of types is
    // described here: https://docs.spade-lang.org/internal/type_representations.html
    fn unsafe_cast<T, O>(in: T) -> O __builtin__

    // Converts a clock signal to a bool by just reinterpreting the bits. This should rarely
    // be used but can be helpful in some situations, such as wanting to output a clock on a
    // pin for debugging
    entity clock_to_bool(c: clock) -> bool {
        unsafe_cast(c)
    }

    // Converts a bool to a clock by just reinterpreting the bits. It is up to the caller
    // to ensure that the result is a valid clock.
    entity bool_to_clock(c: bool) -> clock {
        unsafe_cast(c)
    }
}
